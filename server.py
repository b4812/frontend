from fastapi import FastAPI, UploadFile, File
from starlette.responses import StreamingResponse
from fastapi.responses import FileResponse, Response
import io
import base64
import os
import uuid
import numpy as np
from PIL import Image
import torch
from fastapi.middleware.cors import CORSMiddleware
from u2net_test import main2 as pred

app = FastAPI()

origins = ["http://localhost:3000"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post('/remove_background')
async def remove_background(image: bytes = File(...)):
    array = bytesToArray(image)
    preds = pred(array)
    arrayImg = Image.fromarray(array)
    predsImg = TensorToImage(preds, arrayImg.size).convert("L")
    arrayImg.putalpha(predsImg)
    return ImgToBase64(arrayImg)
    # return Response(arrayImg.tobytes(), media_type='image/png')
    # return Response(content=arrayImg, media_type='image/png')
    # return FileResponse('./test_data/u2net_results/idk1234567.png')


def ImgToBase64(image):
    img_buffer = io.BytesIO()
    image.save(img_buffer, format='PNG')
    byte_data = img_buffer.getvalue()
    base64_str = base64.b64encode(byte_data)
    return base64_str


def bytesToArray(bytes):
    stream =io.BytesIO(bytes)
    array = np.array(Image.open(stream).convert('RGB'))
    return array

def TensorToImage(tensor, img_size):
    predict = tensor
    predict = predict.squeeze()
    predict_np = predict.cpu().data.numpy()
    im = Image.fromarray(predict_np*255).convert('RGB')
    return im.resize((img_size[0], img_size[1]), resample=Image.BILINEAR)
